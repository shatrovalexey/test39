<?php
	/**
	* Сначала нужно создать файл типа .docx в любой офисной программе типа MS Word или Libre Office. В этот файл вставить любой текст (например, абзац из новости), а затем 3 слова или фразы внутри текста заменить метками {slovo_1}, {slovo_2}, {slovo_3}. Это все делаем вручную.
	* Далее нужно сделать PHP-скрипт, в котором будет массив соответствия этих меток и слов для замены. В клиентской части скрипта должна быть кнопка, при нажатии на которую будет скачиваться docx-файл, в котором метки типа {slovo_1} заменены на соответствующие слова автоматически (скриптом; серверная часть).
	* Весь код (и клиентскую и серверную часть) разместить в одном PHP-скрипте.
	* При скачивании браузер должен предлагать сохранить файл с русским названием "Пример файла.docx".
	* Для работы с docx не использовать никакие сторонние библиотеки и классы, только стандартные средства/расширения PHP.
	* В ответе прислать PHP-скрипт и docx-файл.
	*
	* @author Shatrov Aleksej <mail@ashatrov.ru>
	*/

	/**
	* @const CHARSET - кодировка
	* @const MIME_DOCX - MIME-тип docx
	*/
	define( 'CHARSET' , 'utf-8mb4' ) ;
	define( 'MIME_DOCX' , 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' ) ;

	/**
	* Выполнение списка команд в оболочке ОС с проверкой результата
	* @param array $cmds - список команд для выполнения
	* @return array - список строк вывода
	* @throws \Exception
	*/
	function safe_exec( $cmds ) {
		$output = array( ) ;

		foreach ( $cmds as $cmd ) {
			exec( $cmd , $output , $state ) ;

			if ( $state ) {
				throw new \Exception( implode( PHP_EOL , $output ) ) ;
			}
		}

		return $output ;
	}

	/**
	* Создание временных имён файлов и папок
	* @return string - путь к временному файлу или папке
	*/
	function safe_tempnam( ) {
		$result = tempnam( sys_get_temp_dir( ) , '' ) ;
		unlink( $result ) ;

		return $result ;
	}

	/**
	* Проверка HTTP-аргумента 'action'
	*/
	switch( @$_REQUEST[ 'action' ] ) {
		/**
		* Если 'action' = 'upload'...
		*/
		case 'upload' : {

			/**
			* @var $replace array - список замен
			*/
			$replace = array(
				'doc_number' => rand( 1e3 ) ,
				'current_date' => date( 'd.m.Y' )
			) ;

			try {
				$tmpFile = safe_tempnam( ) ;

				if ( ! move_uploaded_file( @$_FILES[ 'file' ][ 'tmp_name' ] , $tmpFile ) ) {
					throw new Exception( 'Ошибка загрузки файла' ) ;
				}

				$outputFileName = 'Название файла.docx' ;

				$tmpDir = safe_tempnam( ) ;
				mkdir( $tmpDir ) ;

				safe_exec( array(
					"unzip '{$tmpFile}' -d '{$tmpDir}'" 
				) ) ;

				foreach ( array( 'document.xml' , 'footer.xml' ) as $fileName ) {
					$docPath = implode( DIRECTORY_SEPARATOR , array( $tmpDir , 'word' , $fileName ) ) ;
					$docSrc = file_get_contents( $docPath ) ;
					$docSrc = preg_replace_callback( '{\$\{(.+)\}}us' , function( $matches ) use( &$replace ) {
						if ( ! isset( $replace[ $matches[ 1 ] ] ) ) {
							return $matches[ 0 ] ;
						}

						return $replace[ $matches[ 1 ] ] ;
					} ) ;
					file_put_contents( $docPath , $docSrc ) ;
				}

				$tmpFile = safe_tempnam( ) ;

				safe_exec( array( "zip '{$tmpFile}' -r '{$tmpDir}'" ) ) ;

				header( 'Content-Type: ' . MIME_DOCX ) ;
				header( 'Content-Dispositon: attachment; filename="' . urlencode( $outputFileName ) . '"' ) ;
				readfile( $tmpFile ) ;
			} catch ( Exception $exception ) {
				header( 'Content-Type: text/plain; charset=' . CHARSET ) ;
				echo $exception->getMessage( ) ;
			}

			@unlink( $tmpFile ) ;
			safe_exec( array( "rm -fr '{$tmpDir}'" ) ) ;
			exit( 0 ) ;
		}
	}
?><!DOCTYPE html>
<html>
	<head>
		<title>Test39</title>
		<meta charset="<?=htmlspecialchars( CHARSET )?>">
		<meta name="Author" content="Shatrov Aleksej <mail@ashatrov.ru>">
	</head>
	<body>
		<form method="post" enctype="multipart/form-data">
			<input type="hidden" name="action" value="upload">
			<label>
				<span>файл docx:</span>
				<input name="file" type="file" accept="<?=htmlspecialchars( MIME_DOCX )?>" required>
			</label>
			<label>
				<span>загрузить</span>
				<input type="submit" value="&rarr;">
			</label>
		</form>
	</body>
</html>